#! /bin/sh

# NOTE use https://www.rstudio.com/products/shiny/download-server/ ?

R -e "shiny::runApp('/app', port=7499, host='0.0.0.0', launch.browser=FALSE)"
